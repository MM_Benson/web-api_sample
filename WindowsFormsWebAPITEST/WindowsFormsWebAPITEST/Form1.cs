﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsWebAPITEST
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CallWebAPI();
        }
private async void CallWebAPI()
{
    HttpClient client = new HttpClient();
    client.DefaultRequestHeaders.Accept.Clear();
    //設定Body內容格式，POST要加上headers
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));            
    //宣告ResponseMessage 來接回傳的內容
    HttpResponseMessage response = client.GetAsync("http://localhost:8055/api/Customers/2").Result;
    if (response.IsSuccessStatusCode)
    {
        List<Customers> customers = new List<Customers>();
        //HttpClient是非同步，await 是為了等待
        string responseBody = await response.Content.ReadAsStringAsync();             
        customers = JsonConvert.DeserializeObject<List<Customers>>(responseBody);
    }
}
        public partial class Customers
        {
            public string CustomerID { get; set; }
            public string CompanyName { get; set; }
            public string ContactName { get; set; }
            public string ContactTitle { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string Region { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
        }
    }
}
