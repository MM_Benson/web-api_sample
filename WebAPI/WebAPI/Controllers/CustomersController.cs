﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;
namespace WebAPI.Controllers
{
    public class CustomersController : ApiController
    {
        //NorthwindEntities db = new NorthwindEntities();

        // GET: api/Customers
        /// <summary>
        /// implement GET
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            NorthwindEntities db = new NorthwindEntities();
            //透過LINQ 取得 NorthwindEntities 的 Customers 資料
            var temp = from r in db.Customers
                       select new
                       {                        
                           ContactName = r.ContactName,
                           CompanyName = r.CompanyName,
                           City = r.City
                       };

            //透過Content 進行 Result Format 
            return Content(HttpStatusCode.OK, temp, Configuration.Formatters.JsonFormatter);
           
        }

        // GET: api/Customers/5
        /// <summary>
        /// implement GET---這邊的summary說明!!顯示於swagger
        /// </summary>
        /// <param name="id">這是參數說明!!顯示於swagger</param>
        /// <returns></returns>
        public  IHttpActionResult Get(int id)
        {
            //透過LINQ 取得 NorthwindEntities 的 Customers 資料
            NorthwindEntities db = new NorthwindEntities();
            var temp = db.Customers; 
            if (id == 1)                             
                return Content(HttpStatusCode.OK, temp, Configuration.Formatters.XmlFormatter);// 進行 Format .Xml            
            else if (id==2)                            
                return Content(HttpStatusCode.OK, temp, Configuration.Formatters.JsonFormatter);//進行 Format Json            
            else if (id==3)
                return Content(HttpStatusCode.OK, id);//回傳整數
            else           
                return Content(HttpStatusCode.OK, id.ToString());//回傳字串            
        }

        // POST: api/Customers
        /// <summary>
        /// implement POST
        /// </summary>
        /// <param name="customers"></param>
        /// <returns></returns>
        public IHttpActionResult Post([FromBody] Customers customers)
        {
            NorthwindEntities db = new NorthwindEntities();
            db.Customers.Add(customers);
            int tmpe = db.SaveChanges();            
            return Content(HttpStatusCode.OK, "OK", Configuration.Formatters.JsonFormatter);
        }

// PUT: api/Customers/5
/// <summary>
/// implement PUT 
/// </summary>
/// <param name="id"></param>
/// <param name="customers"></param>
public IHttpActionResult Put(string id, [FromBody]Customers customers)
{
    NorthwindEntities db = new NorthwindEntities();
    var temp = db.Customers.FirstOrDefault(e => e.CustomerID == id.ToString());    
    temp.Fax = customers.Fax;
    temp.ContactTitle = customers.ContactTitle;
    temp.CompanyName = customers.CompanyName;
    int retValue = db.SaveChanges();
    return Content(HttpStatusCode.OK, "OK", Configuration.Formatters.JsonFormatter);
}

// DELETE: api/Customers/5
/// <summary>
/// implement DELETE
/// </summary>
/// <param name="id"></param>
public IHttpActionResult Delete(string id)
{
    NorthwindEntities db = new NorthwindEntities();
    db.Customers.Remove(db.Customers.FirstOrDefault(e => e.CustomerID == id));
    db.SaveChanges();
    return Content(HttpStatusCode.OK, "OK", Configuration.Formatters.JsonFormatter);
}
    }
}
